#!/usr/bin/ruby

require "json"
require "date"

team = nil
token = nil

if ARGV.size != 1 or ARGV[0] =~ /^-/
  STDERR.puts "Usage #$0 'raw text'"
  exit 1
end

TEXT = ARGV.first

File.open(File.expand_path("~/.idonethis.rc")) do |f|
  f.each_line do |line|
    case line
    when /team\s*=\s*"(.*)"/
      team = $1
    when /token\s*=\s*"(.*)"/
      token = $1
    end
  end
end

raise "Missing token" unless token
raise "Missing team" unless team

pr, pw = IO.pipe

jq = fork do
  pw.close
  STDIN.reopen pr
  exec "jq", "."
end

curl = fork do
  pr.close
  STDOUT.reopen pw

  data = { "raw_text" => TEXT, "team" => team }

  if Time.now.hour < 8 and not TEXT.start_with?("[ ]")
    done_date = Date.today.prev_day.strftime("%Y-%m-%d")
    STDERR.puts "> Set done_date to #{done_date}"
    data["done_date"] = done_date
  end

  exec "curl", "-s",
    "-H", "Authorization: Token #{token}",
    "-H", "Content-Type: application/json",
    "https://idonethis.com/api/v0.1/dones/",
    "-d", data.to_json
end

pr.close
pw.close

Process.waitpid(jq)
Process.waitpid(curl)
